#Log analyzer

##Simple app for viewing exported logs.


Getting started:

1. Open log-analyzer.html in your IDE

2. Paste your logs into the "PLACE LOGS HERE" placeholder

3. If you are having localStorage error in console, then follow this: https://www.chromium.org/for-testers/bug-reporting-guidelines/uncaught-securityerror-failed-to-read-the-localstorage-property-from-window-access-is-denied-for-this-document

4. Use top bar options to navigate through logs :)


Separator keywords note - use this field to define keywords by which log separation of logs should occour (may differ depending on export output)

For example - I'm analizing logs where each of them starts with either "2017-" or "02/28/17", then I set my separatorKeywords to: ['2017-', '02/28/17']


Double click on log to highlight it.