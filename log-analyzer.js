//used regexpes info:
//date regexp - (?<year>\d{4})\W\d{1,2}\W\d{1,2} (yyyy-mm-dd)
//category regexp - (category=)([^\s]+) (category=somecategory)

{
	window.onload = function(){ 
		setOriginalCopy()
		readSavedInputs()
		setClickableLogs()
	}

	let originalLogCopy = null,
			categorySearch = null,
			wordsToHighlight = [],
			config = {}

	function getContent() {
		return $('#logs') //yes, jquery... but library used for marking uses it as well, so no harm here :P
	}

	function setClickableLogs() {
		$('#logs').on('dblclick', '.log', function() {
			$(this).toggleClass('selected')
		})
	}

	function setOriginalCopy() {
		originalLogCopy = getContent().text()
	}

	function toggleExactOption() {
		config.accuracy = (config.accuracy === 'exactly') ? 'partially' : 'exactly'
	}

	function onHighlightRefreshed(doneData) {
		updateCount(doneData)	
	}

	function updateCount(count) {
		$('#words-count').text(count)
	}

	function resetLogs() {
		getContent().text(originalLogCopy)
	}

	function clearValues() {
		$('input').each(function(i, input) {
			$(input).val('')
		})
		saveValues()
	}

	function saveValues() {
		$('input').each(function(i, input) {
		 let $input = $(input)
		 let id = $input.attr('id')
		 let value = $input.val()
		 localStorage.setItem('input-val-' + id, value)
		})
	}

	function readSavedInputs() {
		$('input').each(function(i, input) {
		 let $input = $(input)
		 let id = $input.attr('id')
		 let value = localStorage.getItem('input-val-' + id)

		 if(value) {
			 $input.val(value)
		 }
		})
	}

	function prettify() {
		let values = $('#separator-keys-input').val()
		if(values === '') return

		let separatorKeywords = values.split(' ')
		separatorKeywords.map((keyword) => {
			let oldString = keyword,
			newString = '</div><div class="log">'+keyword,
			newText = '<div class="log">' + getContent().text().replace(RegExp(oldString,"gi"),newString) + '</div>'
			getContent().html(newText)
		})
	}

	function filterByCategory() {
		if($('.log').length === 0) {
			alert('No split logs detected. Please use "Split logs" first before filtering out categories.')
			return
		}

		let category = $('#category-input').val()

		if(category.length === 0) {
			resetLogs()
			prettify()
			return
		}

		$('.log').each(function(i, log){		
			let $log = $(log)
			let reg = new RegExp("(category=" + category + ")([^\s]+)", "g")

			if(!reg.test($log.text())) {
				$log.remove()
			}
		})
	}
	
	function refreshHighlight() {
		let words = $('#words-input').val().split(' '),
				$Content = getContent()
		
		//build array of words to mark from input value
		words.map((word) => {
			if(!wordsToHighlight.includes(word)) {
				wordsToHighlight.push(word)
			}
		})

		//unmark words which where deleted
		wordsToHighlight.map((word, i) => {
			if(!words.includes(word)) {
				wordsToHighlight.splice(i, 1)
				$Content.unmark(word)
			}
		})

		//do actual marking
		$Content.mark(wordsToHighlight, {
			done: onHighlightRefreshed,
			accuracy: config.accuracy || 'partially'
		})
	}
}